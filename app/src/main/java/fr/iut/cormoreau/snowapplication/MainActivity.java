package fr.iut.cormoreau.snowapplication;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public class MainActivity extends ActionBarActivity{
    private ProgressDialog pDialog;
    private EditText input;
    private TextView stateValue;
    private TextView weathervalue;
    private TextView tempMorningValue;
    private TextView tempAfterNValue;
    private TextView windSpeed;
    private TextView snowLevelValue;
    private Switch vibrationSwitch;
    private ImageView logo;
    private int idDrawableLogo;

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        input = (EditText) findViewById(R.id.nameStation);
        Button buttonWeather = (Button) findViewById(R.id.buttonWeather);
        ImageButton openMap = (ImageButton) findViewById(R.id.openMapImg);
        ImageButton openList = (ImageButton) findViewById(R.id.openListButton);
        stateValue = (TextView) findViewById(R.id.stateValue);
        weathervalue = (TextView) findViewById(R.id.weatherValue);
        tempMorningValue = (TextView) findViewById(R.id.tempMorningValue);
        tempAfterNValue = (TextView) findViewById(R.id.tempAfterNValue);
        windSpeed = (TextView) findViewById(R.id.windSpeedValue);
        snowLevelValue = (TextView) findViewById(R.id.snowLevelValue);
        setVibrationSwitch((Switch) findViewById(R.id.vibrationSwitch));
        logo = (ImageView) findViewById(R.id.logoSnow);
        logo.setTag(R.drawable.snowicon);
        idDrawableLogo = (Integer)logo.getTag();
        getVibrationSwitch().setChecked(true);
        openMap.setOnClickListener(openMapClickListener);
        openList.setOnClickListener(openListClickListener);
        buttonWeather.setOnClickListener(buttonClickListener);

        SharedPreferences settings = getSharedPreferences("settings", 0);
        String name = settings.getString("skiResortName","");
        input.setText(name);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("input", input.getText().toString());
        outState.putString("stateValue", stateValue.getText().toString());
        outState.putString("weatherValue", weathervalue.getText().toString());
        outState.putString("tempMorningValue", tempMorningValue.getText().toString());
        outState.putString("tempAfterNValue", tempAfterNValue.getText().toString());
        outState.putString("windSpeed", windSpeed.getText().toString());
        outState.putString("snowLevelValue", snowLevelValue.getText().toString());
    }

    @Override
    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        input.setText((savedInstanceState.getString("input")));
        stateValue.setText(savedInstanceState.getString("stateValue"));
        weathervalue.setText(savedInstanceState.getString("weatherValue"));
        tempMorningValue.setText(savedInstanceState.getString("tempMorningValue"));
        tempAfterNValue.setText(savedInstanceState.getString("tempAfterNValue"));
        windSpeed.setText(savedInstanceState.getString("windSpeed"));
        snowLevelValue.setText(savedInstanceState.getString("snowLevelValue"));
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
            logo.setImageResource(idDrawableLogo);
        else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
            logo.setImageResource(idDrawableLogo);
    }

    View.OnClickListener buttonClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if(input.getText().toString().equals(""))
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.errorToast), Toast.LENGTH_SHORT).show();
            else {
                setpDialog(new ProgressDialog(MainActivity.this));
                getpDialog().setMessage(getResources().getString(R.string.loadingToast));
                String URI = input.getText().toString();
                try {
                    URI = URLEncoder.encode(URI,"UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                new GetWeatherTask(MainActivity.this).execute("http://snowlabri.appspot.com/snow?station=" + URI);
                SharedPreferences settings = getSharedPreferences("settings", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("skiResortName", input.getText().toString());
                editor.apply();
            }
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
        }
    };

    View.OnClickListener openMapClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if(input.getText().toString().equals(""))
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.errorToast), Toast.LENGTH_SHORT).show();
            else {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?q=" + input.getText()));
                startActivity(intent);
            }
        }
    };

    View.OnClickListener openListClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent i = new Intent(MainActivity.this,StationListActivity.class);
            startActivityForResult(i,1);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(android.provider.Settings.ACTION_LOCALE_SETTINGS);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                String result=data.getStringExtra("name");
                input.setText(result);
            }
            if (resultCode == RESULT_CANCELED) {
                input.setText("");
            }
        }
    }//onActivityResult

    public void updateWidgets(String result) {
        if(result == null)
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.serverError), Toast.LENGTH_SHORT).show();
        else{

            JSONObject weatherJSON = null;
            try {
                weatherJSON = new JSONObject(result);
                if(weatherJSON.getString("ouverte").equals("oui"))
                    stateValue.setText(getResources().getString(R.string.stateOpened));
                else
                    stateValue.setText(getResources().getString(R.string.stateClosed));

                if(weatherJSON.getString("temps").equals("beau")){
                    weathervalue.setText(getResources().getString(R.string.weatherSunny));
                    logo.setImageResource(R.drawable.sunny);
                    logo.setTag(R.drawable.sunny);
                    idDrawableLogo = (Integer)logo.getTag();
                }
                else if(weatherJSON.getString("temps").equals("couvert")){
                    weathervalue.setText(getResources().getString(R.string.weatherCloudy));
                    logo.setImageResource(R.drawable.cloudy);
                    logo.setTag(R.drawable.cloudy);
                    idDrawableLogo = (Integer)logo.getTag();
                }
                else if(weatherJSON.getString("temps").equals("neige")){
                    weathervalue.setText(getResources().getString(R.string.weatherSnow));
                    logo.setImageResource(R.drawable.snow);
                    logo.setTag(R.drawable.snow);
                    idDrawableLogo = (Integer)logo.getTag();
                }

                tempMorningValue.setText(weatherJSON.getString("temperatureMatin"));
                tempAfterNValue.setText(weatherJSON.getString("temperatureMidi"));
                windSpeed.setText(weatherJSON.getString("vent"));
                snowLevelValue.setText(weatherJSON.getString("neige"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (weatherJSON != null) {
                Log.i("resJSON", weatherJSON.toString());
            }

        }
    }

    public Switch getVibrationSwitch() {
        return vibrationSwitch;
    }

    public void setVibrationSwitch(Switch vibrationSwitch) {
        this.vibrationSwitch = vibrationSwitch;
    }

    public ProgressDialog getpDialog() {
        return pDialog;
    }

    public void setpDialog(ProgressDialog pDialog) {
        this.pDialog = pDialog;
    }
}
